package com.tsystems.javaschool.tasks.calculator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Alla
 */
public class Validator {

    public static boolean isValid(String input) {
        if (isEmpty(input)) {
            return false;
        }
        String s = input.trim();
        if (!pairedBrackets(s) || !matchesPattern(s)) {
            return false;
        }
        return true;
    }

    private static boolean isEmpty(String input) {
        if (input == null || input.trim().length() == 0) {
            return true;
        }
        return false;
    }

    private static boolean pairedBrackets(String s) {
        int pos = 0;
        int rightBracketCount = 0;
        int leftBracketCount = 0;
        while (pos < s.length()) {
            if (s.charAt(pos) == '(') {
                leftBracketCount++;
            }
            if (s.charAt(pos) == ')') {
                rightBracketCount++;
            }
            pos++;
        }
        return leftBracketCount == rightBracketCount;
    }

    private static boolean matchesPattern(String s) {
        String pstternString = "^(?:(\\()?(\\d+(\\.\\d+)?(\\))?)[*+\\/-])+(\\d+(\\.\\d+)?)(\\))?";
        Pattern pattern = Pattern.compile(pstternString);
        Matcher matcher = pattern.matcher(s);
        return matcher.matches();
    }
}
