package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as
     * decimal mark, parentheses, operations signs '+', '-', '*', '/'<br>
     * Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement
     * is invalid
     */
    public String evaluate(String statement) {
        String result = null;
        if (!Validator.isValid(statement)) {
            return result;
        }
        List<String> list = parseStringToArray(statement.trim());
        if (list.contains("(")) {
            calcInBrackets(list);
        }
        result = calc(list);
        return result;
    }

    private List<String> parseStringToArray(String s) {
        int pos = 0;
        List<String> list = new ArrayList<>();
        String item = "";
        while (pos < s.length()) {
            if (Character.isDigit(s.charAt(pos)) || s.charAt(pos) == '.') {
                item += s.charAt(pos);
            } else {
                if (item.length() > 0) {
                    list.add(item);
                }
                item = Character.toString(s.charAt(pos));
                list.add(item);
                item = "";
            }
            if (pos == s.length() - 1) {
                list.add(item);
            }
            pos++;
        }
        return list;
    }

    private void calcInBrackets(List<String> list) {
        int start = list.indexOf("(");
        int end = list.indexOf(")");
        List<String> subList = list.subList(start + 1, end);
        calc(subList);
        list.remove(list.indexOf("("));
        list.remove(list.indexOf(")"));
    }

    private String calc(List<String> list) {
        ListIterator<String> iter = list.listIterator();
        String item = null;
        while (iter.hasNext()) {
            item = iter.next();
            try {
                hightPriorityCalculaton(iter, item);
            } catch (IllegalArgumentException e) {
                return null;
            }
        }
        iter = list.listIterator();
        while (iter.hasNext()) {
            item = iter.next();
            lowPriorityCalculaton(iter, item);
        }
        return roundResult(list);
    }

    private void hightPriorityCalculaton(ListIterator<String> iter, String item) {
        if (item.equals("*") || item.equals("/")) {
            double result = 0;
            double left = collapseLeft(iter);
            double right = collapseRight(iter);
            switch (item) {
                case "*":
                    result = left * right;
                    break;
                case "/":
                    if (right == 0) {
                        throw new IllegalArgumentException();
                    }
                    result = left / right;
            }
            item = String.valueOf(result);
            iter.add(item);
        }
    }

    private void lowPriorityCalculaton(ListIterator<String> iter, String item) {
        if (item.equals("+") || item.equals("-")) {
            double result = 0;
            double left = collapseLeft(iter);
            double right = collapseRight(iter);
            switch (item) {
                case "+":
                    result = left + right;
                    break;
                case "-":
                    result = left - right;
            }
            item = String.valueOf(result);
            iter.add(item);
        }
    }

    private double collapseLeft(ListIterator<String> iter) {
        iter.previous();
        double left = Double.parseDouble(iter.previous());
        iter.remove(); //remove left item
        iter.next();
        iter.remove(); // remove operator
        return left;
    }

    private double collapseRight(ListIterator<String> iter) {
        double right = Double.parseDouble(iter.next());
        iter.remove(); // remove right item
        return right;
    }

    private String roundResult(List<String> list) {
        String result;
        BigDecimal bd = new BigDecimal(list.get(0));
        double roundedResult = bd.setScale(4, RoundingMode.HALF_UP).doubleValue();
        if (roundedResult == (int) roundedResult) {
            result = String.valueOf((int) roundedResult);
        } else {
            result = String.valueOf(roundedResult);
        }
        return result;
    }

}
