package com.tsystems.javaschool.tasks.subsequence;

import java.util.Comparator;
import java.util.List;
import java.util.ListIterator;

public class ListComparator implements Comparator<List> {

    @Override
    public int compare(List x, List y) {
        if (x.isEmpty()) {
            return 1;
        }
        if (y.isEmpty()) {
            return -1;
        }
        if (!y.containsAll(x)) {
            return -1;
        }

        y.removeIf(item -> !x.contains(item));
        if (x.equals(y)) {
            return 1;
        }
        if (!x.get(0).equals(y.get(0))) {
            return -1;
        }

        ListIterator e1 = x.listIterator();
        ListIterator e2 = y.listIterator();

        while (e1.hasNext() && e2.hasNext()) {
            Object o1 = e1.next();
            Object o2 = e2.next();
            if (!o2.equals(o1)) {
                e1.previous();
                o1 = e1.previous();
                if (!o2.equals(o1)) {
                    return -1;
                }
                e1.next();
            }
        }
        return 1;
    }
}
