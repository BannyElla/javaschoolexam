package com.tsystems.javaschool.tasks.subsequence;

import java.util.Comparator;
import java.util.List;

public class Subsequence {

    public boolean find(List x, List y) {

        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }
        Comparator<List> listCompare = new ListComparator();
        return listCompare.compare(x, y) == 1;
    }
}
