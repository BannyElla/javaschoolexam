package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class PyramidBuilder {

    int listSize;
    int countItemsConsumed;
    int countLevel;
    int subArraySize;

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line
     * and maximum at the bottom, from left to right). All vacant positions in
     * the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be
     * build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }
        listSize = inputNumbers.size();
        countItemsConsumed = 0;
        countLevel = 0;
        subArraySize = 0;
        
        if (!canBuld()) {
            throw new CannotBuildPyramidException();
        }
        Set<Integer> sortAndUnique = new TreeSet<>(inputNumbers);
        Deque<Integer> stack = new ArrayDeque<>(sortAndUnique);

        return building(stack);

    }


    private int[][] building(Deque<Integer> stack) {       
        subArraySize = countLevel + (countLevel - 1);
        int[][] arr = new int[countLevel][subArraySize];

        int i = 1;
        int startPose = (int) Math.floor((subArraySize) / 2);
        while (stack.size() > 0) {
            int pos = startPose - i + 1;
            for (int j = 0; j < i; j++) {
                arr[i - 1][pos] = stack.pollFirst();
                pos = pos + 2;
            }
            i++;
        }
        return arr;
    }

    private boolean canBuld() {
        int i = 1;       
        while (true) {
            countItemsConsumed += i;
            if (countLevel == Integer.MAX_VALUE) {
                return false;
            }
            countLevel++;
            if (countItemsConsumed >= listSize) {
                break;
            }
            i++;
        }
        if (countItemsConsumed == listSize) {
            return true;
        }        
        return false;
    }

}
